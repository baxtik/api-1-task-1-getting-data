package ru.baxtiyor.web.api.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.baxtiyor.web.api.converter.VideoToDtoConverter;
import ru.baxtiyor.web.api.dto.VideoDto;
import ru.baxtiyor.web.entity.Video;
import ru.baxtiyor.web.repository.VideoRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class VideoService {
    private final VideoRepository repository;
    private final VideoToDtoConverter converter;

    @Transactional
    public VideoDto getResult(Long id, String sourceDataUrl, String tokenDataUrl) {
        Optional<Video> videoOptional = repository.findById(id);
        Video video;
        if (videoOptional.isPresent()) {
            video = videoOptional.get();
        } else {
            video = new Video();
            video.setId(id);
        }
        video.setSourceDataUrl(sourceDataUrl);
        video.setTokenDataUrl(tokenDataUrl);
        Video resultVideo = repository.saveAndFlush(video);
        return converter.convert(resultVideo);
    }
}
