package ru.baxtiyor.web.api.converter;

import lombok.SneakyThrows;
import org.json.JSONObject;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.baxtiyor.web.api.dto.VideoDto;
import ru.baxtiyor.web.api.json.JsonReader;
import ru.baxtiyor.web.api.json.VideoResponse;

import java.io.IOException;

@Component
public class VideoToResponseConverter implements Converter<VideoDto, VideoResponse> {
    @SneakyThrows
    @Override
    public VideoResponse convert(VideoDto videoDto) {
        return new VideoResponse(
                videoDto.getId(),
                getUrlType(videoDto.getSourceDataUrl()),
                getVideoUrl(videoDto.getSourceDataUrl()),
                getValue(videoDto.getTokenDataUrl()),
                getTtl(videoDto.getTokenDataUrl())
        );
    }

    private String getUrlType(String url) throws IOException {
        return jsonObject(url).getString("urlType");
    }

    private String getVideoUrl(String url) throws IOException {
        return jsonObject(url).getString("videoUrl");
    }

    private String getValue(String url) throws IOException {
        return jsonObject(url).getString("value");
    }

    private Long getTtl(String url) throws IOException {
        return jsonObject(url).getLong("ttl");
    }

    private JSONObject jsonObject(String url) throws IOException {
        return JsonReader.readJsonFromUrl(url);
    }
}
