package ru.baxtiyor.web.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.baxtiyor.web.api.dto.VideoDto;
import ru.baxtiyor.web.entity.Video;

@Component
public class VideoToDtoConverter implements Converter<Video, VideoDto> {
    @Override
    public VideoDto convert(Video video) {
        return new VideoDto(
                video.getId(),
                video.getSourceDataUrl(),
                video.getTokenDataUrl()
        );
    }
}
