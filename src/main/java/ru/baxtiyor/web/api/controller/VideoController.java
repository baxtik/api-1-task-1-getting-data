package ru.baxtiyor.web.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.baxtiyor.web.api.converter.VideoToResponseConverter;
import ru.baxtiyor.web.api.dto.VideoDto;
import ru.baxtiyor.web.api.json.VideoRequest;
import ru.baxtiyor.web.api.json.VideoResponse;
import ru.baxtiyor.web.api.service.VideoService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class VideoController {
    private final VideoService videoService;
    private final VideoToResponseConverter converter;

    @PostMapping("video")
    public ResponseEntity<List<VideoResponse>> getResult(@RequestBody @Valid List<VideoRequest> videoRequests) {
        List<VideoResponse> responseList = new ArrayList<>();
        for (VideoRequest request : videoRequests) {
            VideoDto videoDto = videoService.getResult(
                    request.getId(),
                    request.getSourceDataUrl(),
                    request.getTokenDataUrl()
            );
            responseList.add(converter.convert(videoDto));
        }
        return ResponseEntity.ok(responseList);
    }
}
