package ru.baxtiyor.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VideoResponse {
    private long id;
    private String urlType;
    private String videoUrl;
    private String value;
    private long ttl;
}
