package ru.baxtiyor.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VideoRequest {
    private long id;
    @NotNull
    private String sourceDataUrl;
    @NotNull
    private String tokenDataUrl;
}
