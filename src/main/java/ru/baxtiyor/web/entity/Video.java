package ru.baxtiyor.web.entity;

import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "video")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Video {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "source_Data_Url")
    private String sourceDataUrl;

    @Column(name = "token_Data_Url")
    private String tokenDataUrl;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
