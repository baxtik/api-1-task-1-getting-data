package ru.baxtiyor.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.baxtiyor.web.entity.Video;

public interface VideoRepository extends JpaRepository<Video, Long> {
}
