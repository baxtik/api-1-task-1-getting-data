package ru.baxtiyor.web.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.baxtiyor.web.api.converter.VideoToResponseConverter;
import ru.baxtiyor.web.api.dto.VideoDto;
import ru.baxtiyor.web.api.json.VideoRequest;
import ru.baxtiyor.web.api.json.VideoResponse;
import ru.baxtiyor.web.api.service.VideoService;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.util.Arrays.asList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(VideoController.class)
@RunWith(SpringRunner.class)
class VideoControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    VideoService videoService;
    @SpyBean
    VideoToResponseConverter converter;

    @BeforeEach
    void setUp() {
        when(videoService.getResult(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s"))
                .thenReturn(new VideoDto(
                                1L,
                                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s"
                        )
                );
    }

    @Test
    void getResult() throws Exception {
        mockMvc.perform(post("/api/video")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(videoRequests()))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(asJsonString(videoResponses())));

    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<VideoRequest> videoRequests() {
        VideoRequest request = new VideoRequest(
                1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s"
        );
        List<VideoRequest> result = new ArrayList<>();
        result.add(request);
        return result;
    }

    private List<VideoResponse> videoResponses() {
        VideoResponse response = new VideoResponse(
                1L,
                "LIVE",
                "rtsp://127.0.0.1/1",
                "fa4b588e-249b-11e9-ab14-d663bd873d93",
                120
        );
        List<VideoResponse> result = new ArrayList<>();
        result.add(response);
        return result;
    }

}