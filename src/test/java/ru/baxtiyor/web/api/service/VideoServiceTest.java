package ru.baxtiyor.web.api.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.baxtiyor.web.api.converter.VideoToDtoConverter;
import ru.baxtiyor.web.api.dto.VideoDto;
import ru.baxtiyor.web.entity.Video;
import ru.baxtiyor.web.repository.VideoRepository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class VideoServiceTest {
    @InjectMocks
    VideoService subj;
    @Mock
    VideoRepository repository;
    @Mock
    VideoToDtoConverter converter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getResultSuccessfully() {
        when(repository.saveAndFlush(getVideoObject())).thenReturn(getVideoObject());
        when(converter.convert(getVideoObject())).thenReturn(getVideoDto());
        when(subj.getResult(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s")).thenReturn(getVideoDto());
        VideoDto videoDto = subj.getResult(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s");

        assertNotNull(videoDto);

        verify(repository, times(2)).saveAndFlush(any());
        verify(converter, times(1)).convert(any());
    }

    @Test
    void getResultNotSuccessfully() {
        when(repository.saveAndFlush(getVideoObject())).thenReturn(getVideoObject());
        when(converter.convert(getVideoObject())).thenReturn(getVideoDto());
        when(subj.getResult(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s")).thenReturn(null);
        VideoDto videoDto = subj.getResult(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s");

        assertNull(videoDto);

        verify(repository, times(2)).saveAndFlush(any());
        verify(converter, times(1)).convert(any());
    }

    private Video getVideoObject() {
        return new Video(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s");
    }

    private VideoDto getVideoDto() {
        return new VideoDto(1L,
                "http://www.mocky.io/v2/5c51b230340000094f129f5d",
                "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s");
    }


}